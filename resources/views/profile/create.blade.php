@extends('app')
@section('js')
    <script src="{!! asset('js/create-profile.js') !!}"/>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Crear personaje</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/create') }}">
                            {!! csrf_field() !!}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Nombre Personaje</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="nombre_personaje"
                                               value="{{ old('nombre_personaje') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Nombre jugador</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="nombre_jugador"
                                               value="{{ old('nombre_jugador') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Alineamiento</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="alineamiento"
                                               value="{{ old('alineamiento') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Nivel</label>
                                    <div class="col-md-8">
                                        <input type="number" class="form-control" name="nivel"
                                               value="{{ old('nivel') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Hogar</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="hogar" value="{{ old('hogar') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Deidad</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="deidad"
                                               value="{{ old('deidad') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Tamaño</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="tamano"
                                               value="{{ old('tamano') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Sexo</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="sexo" value="{{ old('sexo') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Edad</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="edad" value="{{ old('edad') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Altura</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="altura"
                                               value="{{ old('altura') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Peso</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="peso" value="{{ old('peso') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Cabello</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="cabello"
                                               value="{{ old('cabello') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Ojos</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="ojos" value="{{ old('ojos') }}">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-2">
                                        <label for="raza">Raza:</label>
                                        <select class="form-control" id="raza" name="raza">
                                            <option value="elfo">Elfo</option>
                                            <option value="enano">Enano</option>
                                            <option value="gnomo">Gnomo</option>
                                            <option value="humano">Humano</option>
                                            <option value="mediano">Mediano</option>
                                            <option value="semielfo">Semielfo</option>
                                            <option value="semiorco">Semiorco</option>
                                        </select>
                                        <div class="imagenes-razas text-center">
                                            <img src="" alt="">
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection